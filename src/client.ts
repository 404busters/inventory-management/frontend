import axios from 'axios';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';

export const restfulClient = axios.create({
  baseURL: `${process.env.VUE_APP_API_HOST || ''}/api/v1`,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const graphqlClient = new ApolloClient({
  link: new HttpLink({
    uri: `${process.env.VUE_APP_API_HOST || ''}/graphql`,
  }),
  cache: new InMemoryCache(),
});

export default restfulClient;
