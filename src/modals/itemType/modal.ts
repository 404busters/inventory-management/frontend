export default interface Modal {
    id: string;
    name: string;
    description: string;
}
