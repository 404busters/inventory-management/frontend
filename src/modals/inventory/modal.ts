export enum InventoryStatus {
    Stock = 'STOCK',
    InUse = 'IN_USE',
    Repair = 'REPAIR',
}

export interface Inventory {
    id: string;
    itemType: string;
    location: string;
    status: InventoryStatus;
}
