import { Inventory, InventoryStatus } from './modal';
import { restfulClient as client } from '@/client';
import Repository from '../repository';

interface InventoryInput {
  itemType: string;
  location: string;
  status: InventoryStatus;
}

const repository: Repository<Inventory, InventoryInput> = {
  list() {
    return Promise.reject(new Error('list of inventory is not implement'));
  },
  get(id: string) {
    return client.get(`inventory/${id}`).then(({ data: { data } }) => data as Inventory);
  },
  create(input: InventoryInput) {
    return client.post('inventory', input).then(({ data: { data } }) => data as Inventory);
  },
  update(id: string, input: InventoryInput): Promise<Inventory> {
    return client.patch(`inventory/${id}`, input).then(({ data: { data } }) => data as Inventory);
  },
  delete(id: string) {
    return client.delete(`inventory/${id}`);
  },
};

export default repository;
