import Vue from 'vue';
import Vuex, { Store } from 'vuex';

Vue.use(Vuex);

const CHANGE_AUTH = 'CHANGE_AUTH';

export const store = new Store({
  state: {
    username: null,
    password: null,
  },
  mutations: {
    [CHANGE_AUTH](state, { username, password }) {
      state.username = username;
      state.password = password;
    },
  },
  actions: {
    setAuth({ commit }, { username, password }) {
      commit(CHANGE_AUTH, { username, password });
    },
  },
});
